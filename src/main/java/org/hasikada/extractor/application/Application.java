package org.hasikada.extractor.application;

import org.hasikada.extractor.config.ApplicationConfig;
import org.hasikada.extractor.convert.IntegerConverter;
import org.hasikada.extractor.extract.Extractor;
import org.hasikada.extractor.filter.PrimeNumberFilter;
import org.hasikada.extractor.present.Presenter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration;
import org.springframework.context.annotation.Import;

import java.io.File;

/**
 * Main application class for running extractor as a standalone application.
 *
 * @author Adam Hasik
 */
@Import({
        ApplicationConfig.class
})
@SpringBootApplication (exclude = SecurityAutoConfiguration.class)
public class Application implements CommandLineRunner {


    private final Extractor extractor;
    private final IntegerConverter converter;
    private final PrimeNumberFilter filter;
    private final Presenter presenter;

    @Autowired
    public Application(
            Extractor extractor,
            IntegerConverter converter,
            PrimeNumberFilter filter,
            Presenter presenter) {
        this.extractor = extractor;
        this.converter = converter;
        this.filter = filter;
        this.presenter = presenter;
    }

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

    @Override
    public void run(String... args) throws Exception {
        try {
            if (args.length == 1) {
                etl(new File(args[0]));
                return;
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

        help();
    }

    void etl(File dataFile) throws Exception {
        extractor.extract(dataFile)
            .stream()
            .map(converter::convert)
            .filter(filter::isPrime)
            .forEach(presenter::present);
    }

    private void help() {
        System.out.println("Usage: java xlsx-extractor [FILE]");
    }

}
