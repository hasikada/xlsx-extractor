package org.hasikada.extractor.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Application configuration.
 *
 * @author Adam Hasik
 */
@Configuration
@ComponentScan({
        "org.hasikada.extractor.extract",
        "org.hasikada.extractor.convert",
        "org.hasikada.extractor.filter",
        "org.hasikada.extractor.present"
})
public interface ApplicationConfig {

}
