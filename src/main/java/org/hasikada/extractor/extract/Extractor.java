package org.hasikada.extractor.extract;

import java.io.File;
import java.util.List;

/**
 * Data extractor.
 *
 * @author Adam Hasik
 */
public interface Extractor {

    // TODO non null checker
    /**
     * Extracts a data from provided source
     *
     * @param file source file
     * @return extracted data
     *
     * @throws NullPointerException if the {@code path} argument is {@code null}
     * @throws Exception in case of error
     */
    List<String> extract(File file) throws Exception;

}
