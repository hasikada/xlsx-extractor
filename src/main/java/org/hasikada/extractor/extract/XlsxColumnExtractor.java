package org.hasikada.extractor.extract;

import static java.lang.String.format;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.exceptions.NotOfficeXmlFileException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

// TODO
/**
 * Extracts B column from xlsx spreadsheet.
 *
 * @author Adam Hasik
 */
@Component
public class XlsxColumnExtractor implements Extractor {

    // TODO non null checker
    @Override
    public List<String> extract(File dataFile) throws Exception {

        List<String> data = new ArrayList<>();

        try (XSSFWorkbook workbook = new XSSFWorkbook(dataFile)) {
            XSSFSheet sheet = workbook.getSheetAt(0);

            for (Row row : sheet) {
                Cell cell = row.getCell(1);
                data.add(cell.getStringCellValue());
            }
        } catch (InvalidFormatException | NotOfficeXmlFileException e) {
            throw new Exception(format("Invalid data format '%s'!", dataFile), e);
        } catch (IOException e) {
            throw new Exception(format("Cannot open file '%s'!", dataFile), e);
        }

        return data;
    }

}
