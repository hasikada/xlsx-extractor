package org.hasikada.extractor.convert;

import static java.util.Objects.isNull;
import static org.apache.commons.lang3.StringUtils.trim;

import org.springframework.stereotype.Component;

@Component
public class IntegerConverterImpl implements IntegerConverter {

    @Override
    public Integer convert(String value) {
        try {
            return isNull(value) ? null : Integer.valueOf(trim(value));
        } catch (NumberFormatException e) {
            return null;
        }
    }

}
