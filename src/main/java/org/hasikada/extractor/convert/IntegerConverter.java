package org.hasikada.extractor.convert;

/**
 * Integer converter.
 *
 * @author Adam Hasik
 */
public interface IntegerConverter {

    /**
     * Converts a string value to integer.
     *
     * @param value given value
     * @return converted integer or {@code null}
     */
    Integer convert(String value);

}
