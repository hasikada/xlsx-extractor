package org.hasikada.extractor.present;

/**
 * Presents a data.
 *
 * @author Adam Hasik
 */
public interface Presenter {

    /**
     * Presents the given data.
     *
     * @param value given data
     */
    void present(Object value);

}
