package org.hasikada.extractor.present;

import org.springframework.stereotype.Component;

/**
 * Displays a data on standard output.
 */
@Component
public class SimplePresenter implements Presenter {

    @Override
    public void present(Object value) {
        System.out.println(value);
    }


}
