package org.hasikada.extractor.filter;

import static java.util.Objects.isNull;

import org.apache.commons.math3.primes.Primes;
import org.springframework.stereotype.Component;

@Component
public class PrimeNumberFilterImpl implements PrimeNumberFilter {

    @Override
    public boolean isPrime(Integer value) {
        return isNull(value) ? false : Primes.isPrime(value);
    }

}
