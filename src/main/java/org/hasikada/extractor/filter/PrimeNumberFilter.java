package org.hasikada.extractor.filter;

/**
 * Prime number filter.
 *
 * @author Adam Hasik
 */
public interface PrimeNumberFilter {

    /**
     * Verifies a value is prime number.
     *
     * @param value integer value to check
     * @return true is value is prime number otherwise false
     */
    boolean isPrime(Integer value);

}
