package org.hasikada.extractor.filter;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.hasikada.extractor.filter.PrimeNumberFilter;
import org.hasikada.extractor.filter.PrimeNumberFilterImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(DataProviderRunner.class)
public class PrimeNumberFilterImplTest {

    private PrimeNumberFilter filter;

    @Before
    public void setUp() {
        filter = new PrimeNumberFilterImpl();
    }

    @DataProvider
    public static Object[][] isPrimeData() {
        return new Object[][] {
            {null, false},
            {-1, false},
            {0, false},
            {1, false},
            {2, true},
            {5645641, false},
            {5645657, true},
            {5799555, false},
            {15619, true},
            {5221652, false},
            {1234187, true},
            {9584, false},
            {211, true},
            {7, true},
            {9785, false},
            {65132114, false},
            {9788677, true},
            {23311, true},
            {54881, true},
            {21448, false},
            {28, false},
            {564564556, false}
        };
    }


    @Test
    @UseDataProvider("isPrimeData")
    public void isPrime(Integer value, boolean result) {
        assertThat(filter.isPrime(value), is(result));
    }

}
