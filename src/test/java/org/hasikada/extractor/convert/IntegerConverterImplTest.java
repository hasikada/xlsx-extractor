package org.hasikada.extractor.convert;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(DataProviderRunner.class)
public class IntegerConverterImplTest {

    private IntegerConverter converter;

    @Before
    public void setUp() {
        converter = new IntegerConverterImpl();
    }

    @DataProvider
    public static Object[][] convertData() {
        return new Object[][] {
            {null, null},
            {"", null},
            {"10", 10},
            {" 10", 10},
            {"10L", null},
            {"10.0", null},
            {"1.5", null}
        };
    }


    @Test
    @UseDataProvider("convertData")
    public void convert(String value, Integer result) {
        assertThat(converter.convert(value), is(result));
    }

}
