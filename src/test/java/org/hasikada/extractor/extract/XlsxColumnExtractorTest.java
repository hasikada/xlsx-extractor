package org.hasikada.extractor.extract;

import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.springframework.util.ResourceUtils.getFile;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

@RunWith(DataProviderRunner.class)
public class XlsxColumnExtractorTest {

    Extractor extractor;

    @Before
    public void setUp() {
        extractor = new XlsxColumnExtractor();
    }

    @DataProvider
    public static Object[][] extractData() {
        return new Object[][] {
            {
                "classpath:data.xlsx", asList(
                        "Data","5645641","5645657","5799555","15619","5221652","1234187","9584","211","7","9785",
                        "65132114","9788677","23311","54881","21448","28","564564556")
            },
            {
                "classpath:invalid-data.xlsx", asList(
                        "", "5645657", "", "15619", "5221652", "", "", "B")
            }

        };
    }

    @Test
    @UseDataProvider("extractData")
    public void extract(String path, List<String> data) throws Exception {
        assertThat(extractor.extract(getFile(path)), is(data));
    }

    @Test(expected = Exception.class)
    public void extractInvalidFormat() throws Exception {
        extractor.extract(getFile("classpath:data.txt"));
    }

}
