package org.hasikada.extractor.config;

import static org.junit.Assert.assertNotNull;

import org.hasikada.extractor.convert.IntegerConverter;
import org.hasikada.extractor.extract.Extractor;
import org.hasikada.extractor.filter.PrimeNumberFilter;
import org.hasikada.extractor.present.Presenter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ApplicationConfig.class})
public class ApplicationConfigTest {

    @Autowired
    Extractor extractor;

    @Autowired
    IntegerConverter converter;

    @Autowired
    PrimeNumberFilter filter;

    @Autowired
    Presenter presenter;

    @Test
    public void testSpringContext() {
        assertNotNull("Extractor must be autowired", extractor);
        assertNotNull("Converter must be autowired,", converter);
        assertNotNull("Filter must be autowired", filter);
        assertNotNull("Presenter must be autowired", presenter);
    }

}
