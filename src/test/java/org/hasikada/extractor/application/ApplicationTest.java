package org.hasikada.extractor.application;

import static java.util.Collections.singletonList;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.hasikada.extractor.convert.IntegerConverter;
import org.hasikada.extractor.extract.Extractor;
import org.hasikada.extractor.filter.PrimeNumberFilter;
import org.hasikada.extractor.present.Presenter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.File;

@RunWith(MockitoJUnitRunner.class)
public class ApplicationTest {

    private static final File DATA_FILE = null;
    private static final String VALUE = "2";
    private static final Integer PRIME_NUMBER = 2;
    @Mock
    Extractor extractor;
    @Mock
    IntegerConverter converter;
    @Mock
    PrimeNumberFilter filter;
    @Mock
    Presenter presenter;

    Application application;

    @Before
    public void setUp() throws Exception {
        when(extractor.extract(DATA_FILE)).thenReturn(singletonList(VALUE));
        when(converter.convert(VALUE)).thenReturn(PRIME_NUMBER);
        when(filter.isPrime(PRIME_NUMBER)).thenReturn(true);

        application = new Application(extractor, converter, filter, presenter);
    }

    @Test
    public void etl() throws Exception {
        application.etl(DATA_FILE);

        verify(extractor).extract(DATA_FILE);
        verify(converter).convert(VALUE);
        verify(filter).isPrime(PRIME_NUMBER);
        verify(presenter).present(PRIME_NUMBER);
    }

}
