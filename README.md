# Data Extractor
Extracts data from Excel 2003-2013 (xlsx) spreadsheet. The data are located in first sheet in column B. The date are stored as string field.

## Testing
Run unit tests
	
	mvn clean test

Run application

	mvn spring-boot:run

## Run application
Create application package

	mvn package

Run standalone application

	java -jar target/xlsx-extractor-0.0.1-SNAPSHOT.jar FILE

## Limitation
The application is not adapted to handle the big spreadsheet file. You might observe OOM situation in such a case.
